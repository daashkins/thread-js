import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Segment } from 'semantic-ui-react';
import { toggleUpdatedPost, updatePost } from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Spinner from 'src/components/Spinner';
import UpdatePost from 'src/components/UpdatePost';

const UpdatedPost = ({
  post,
  toggleUpdatedPost: toggle,
  updatePost: changePost
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {post
      ? (
        <Modal.Content>
          <Post
            post={post}
            toggleUpdatedPost={toggle}
          />
          <Segment>
            <UpdatePost updatePost={changePost} postId={post.id} />
          </Segment>
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

UpdatedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleUpdatedPost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.updatedPost
});

const actions = { toggleUpdatedPost, updatePost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdatedPost);
