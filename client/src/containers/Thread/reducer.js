import {
  SET_ALL_POSTS,
  LOAD_MORE_POSTS,
  ADD_POST,
  SET_EXPANDED_POST,
  SET_UPDATED_POST,
  UPDATE_POST
} from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_ALL_POSTS:
      return {
        ...state,
        posts: action.posts,
        hasMorePosts: Boolean(action.posts.length)
      };
    case LOAD_MORE_POSTS:
      return {
        ...state,
        posts: [...(state.posts || []), ...action.posts],
        hasMorePosts: Boolean(action.posts.length)
      };
    case ADD_POST:
      return {
        ...state,
        posts: [action.post, ...state.posts]
      };
    case UPDATE_POST:
      return state.posts.map(post => {
        if (post.id !== action.post.id) {
          // This isn't the item we care about - keep it as-is
          return post;
        }

        // Otherwise, this is the one we want - return an updated value
        return {
          ...post,
          ...action.post
        };
      });

    case SET_EXPANDED_POST:
      return {
        ...state,
        expandedPost: action.post
      };
    case SET_UPDATED_POST:
      return {
        ...state,
        updatedPost: action.post
      };
    default:
      return state;
  }
};
