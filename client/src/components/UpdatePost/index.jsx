import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button } from 'semantic-ui-react';

const UpdatePost = ({
  postId,
  updatePost
}) => {
  const [body, setBody] = useState('');

  const handleUpdatePost = async () => {
    if (!body) {
      return;
    }
    await updatePost({ postId, body });
    setBody('');
  };

  return (
    <Form reply onSubmit={handleUpdatePost}>
      <Form.TextArea
        value={body}
        placeholder="Update your post..."
        onChange={ev => setBody(ev.target.value)}
      />
      <Button type="submit" content="Update" labelPosition="left" icon="edit" primary />
    </Form>
  );
};

UpdatePost.propTypes = {
  updatePost: PropTypes.func.isRequired,
  postId: PropTypes.string.isRequired
};

export default UpdatePost;
